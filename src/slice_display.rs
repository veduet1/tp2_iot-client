use std::fmt::{Display, Formatter, Write};

#[derive(Debug)]
pub struct SliceDisplay<'a>(&'a [u8]);

impl<'a> SliceDisplay<'a> {
    pub fn new(slice: &'a [u8]) -> Self {
        Self(slice)
    }
}

impl<'a> Display for SliceDisplay<'a> {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        for i in self.0 {
            let c = char::from(*i);

            if !c.is_control() {
                f.write_char(c)?;
            }
        }

        Ok(())
    }
}

#[cfg(test)]
mod tests {
    use crate::slice_display::SliceDisplay;

    #[test]
    fn basic() {
        let test = b"Hello world";

        let output = format!("{}", SliceDisplay::new(test.as_slice()));

        assert_eq!(test.as_slice(), output.as_bytes())
    }

    #[test]
    fn special() {
        let test = b"\0\tHe\r\rllo world";

        let output = format!("{}", SliceDisplay::new(test.as_slice()));

        assert_eq!("Hello world", output.as_str())
    }
}
