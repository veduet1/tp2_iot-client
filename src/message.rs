use serde::Deserialize;

use crate::hexbuf::HexBuf;
use base64_serde::base64_serde_type;
use crate::packet::RawPacket;
use crate::error::Result;

base64_serde_type!(Base64Std, base64::engine::general_purpose::STANDARD);

#[derive(Deserialize, Debug)]
pub struct Message {
    pub app_eui: HexBuf<8>,
    pub dev_eui: HexBuf<8>,

    #[serde(with = "Base64Std")]
    pub payload: Vec<u8>,
}

impl Message {
    pub fn decrypt_data(&self, key : &[u8; 16]) -> Result<Vec<u8>> {
        let mut iv = [0u8; 16];

        iv.get_mut(0..8).unwrap().copy_from_slice(self.dev_eui.as_slice());
        iv.get_mut(8..16).unwrap().copy_from_slice(self.app_eui.as_slice());

        let raw = RawPacket::try_from(self.payload.as_slice())?;

        raw.decrypt_data(key, &iv)
    }
}

#[cfg(test)]
mod tests {
    use super::Message;
    use serde_json::from_slice;

    #[test]
    fn basic() {
        let raw = b"{\"app_eui\":\"6948A4B59F4666AD\",\"dev_eui\":\"1C29FBD1D2084E21\",\"payload\":\"SGVsbG8sIHdvcmxkIQ==\"}";

        let message: Message = from_slice(raw.as_slice()).unwrap();

        assert_eq!(
            message.app_eui,
            [0x69, 0x48, 0xA4, 0xB5, 0x9F, 0x46, 0x66, 0xAD]
        );
        assert_eq!(
            message.dev_eui,
            [0x1C, 0x29, 0xFB, 0xD1, 0xD2, 0x08, 0x4E, 0x21]
        );

        assert_eq!(
            message.payload,
            [0x48, 0x65, 0x6C, 0x6C, 0x6F, 0x2C, 0x20, 0x77, 0x6F, 0x72, 0x6C, 0x64, 0x21]
        )
    }

    #[test]
    fn with_unknown_fields() {
        let raw = b"{\"app_eui\":\"6948A4B59F4666AD\",\"dev_eui\":\"1C29FBD1D2084E21\",\"payload\":\"SGVsbG8sIHdvcmxkIQ==\", \"name\":\"Test\"}";

        let _message: Message = from_slice(raw.as_slice()).unwrap();
    }
}
