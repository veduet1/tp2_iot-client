use std::fmt::Formatter;
use std::ops::Deref;

use serde::de::{Error, Unexpected, Visitor as VisitorTrait};
use serde::{Deserialize, Deserializer};

pub fn from_hex_digit(from: char) -> Option<u8> {
    let from = from as u8;

    if (b'0'..=b'9').contains(&from) {
        Some(from - b'0')
    } else if (b'a'..=b'f').contains(&from) {
        Some(from - b'a' + 10)
    } else if (b'A'..=b'F').contains(&from) {
        Some(from - b'A' + 10)
    } else {
        None
    }
}

#[derive(Debug, PartialEq)]
pub struct HexBuf<const LEN: usize>([u8; LEN]);

impl<const LEN: usize> HexBuf<LEN> {
    pub const fn new(data: [u8; LEN]) -> Self {
        HexBuf(data)
    }
}

impl<'de, const LEN: usize> Deserialize<'de> for HexBuf<LEN> {
    fn deserialize<D>(deserializer: D) -> Result<Self, D::Error>
    where
        D: Deserializer<'de>,
    {
        struct Visitor<const LEN: usize>();

        impl<'de, const LEN: usize> VisitorTrait<'de> for Visitor<LEN> {
            type Value = HexBuf<LEN>;

            fn expecting(&self, formatter: &mut Formatter) -> std::fmt::Result {
                write!(formatter, "a hex number of {} hex digits (0-F)", LEN * 2)
            }

            fn visit_str<E>(self, v: &str) -> Result<Self::Value, E>
            where
                E: Error,
            {
                let length = v.len() / 2;
                let additional = v.len() % 2;

                let total_length = length + additional;

                let mut ret = [0u8; LEN];

                if total_length > LEN {
                    return Err(E::invalid_length(v.len(), &self));
                }

                let mut chars = v.chars();
                let mut idx = 0;

                if additional > 0 {
                    if let Some(c) = chars.next() {
                        if let Some(v) = from_hex_digit(c) {
                            ret[idx] = v;
                            idx += 1;
                        } else {
                            return Err(E::invalid_value(Unexpected::Str(v), &self));
                        }
                    }
                }

                let mut buffer = 0u8;
                let mut buffer_end = false;

                while let Some(c) = chars.next() {
                    if let Some(v) = from_hex_digit(c) {
                        if buffer_end {
                            ret[idx] = buffer | v;

                            idx += 1;
                        } else {
                            buffer = v << 4;
                        }

                        buffer_end = !buffer_end;
                    } else {
                        return Err(E::invalid_value(Unexpected::Str(v), &self));
                    }
                }

                Ok(HexBuf(ret))
            }

            fn visit_string<E>(self, v: String) -> Result<Self::Value, E>
            where
                E: Error,
            {
                self.visit_str(v.as_str())
            }
        }

        deserializer.deserialize_str(Visitor())
    }
}

impl<const LEN: usize> Deref for HexBuf<LEN> {
    type Target = [u8; LEN];

    fn deref(&self) -> &Self::Target {
        &self.0
    }
}

impl<const LEN: usize> PartialEq<[u8; LEN]> for HexBuf<LEN> {
    fn eq(&self, other: &[u8; LEN]) -> bool {
        self.0.eq(other)
    }
}

#[cfg(test)]
mod tests {
    use crate::hexbuf::{from_hex_digit, HexBuf};
    use serde_test::{assert_de_tokens, assert_de_tokens_error, Token};

    #[test]
    fn basic() {
        assert_de_tokens(&HexBuf([0x56, 0x3D, 0x47, 0x00]), &[Token::Str("563D4700")]);
    }

    #[test]
    fn partial() {
        assert_de_tokens(&HexBuf([0x05, 0x31, 0x40]), &[Token::Str("53140")]);
    }

    #[test]
    fn length_error() {
        assert_de_tokens_error::<HexBuf<1>>(
            &[Token::Str("0105")],
            "invalid length 4, expected a hex number of 2 hex digits (0-F)",
        );
    }

    #[test]
    fn content_error() {
        assert_de_tokens_error::<HexBuf<3>>(
            &[Token::Str("3F6C5N")],
            "invalid value: string \"3F6C5N\", expected a hex number of 6 hex digits (0-F)",
        );
    }

    #[test]
    fn base_hex_digit() {
        assert_eq!(from_hex_digit('0'), Some(0));
        assert_eq!(from_hex_digit('9'), Some(9));

        assert_eq!(from_hex_digit('a'), Some(10));
        assert_eq!(from_hex_digit('f'), Some(15));

        assert_eq!(from_hex_digit('A'), Some(10));
        assert_eq!(from_hex_digit('F'), Some(15));
    }

    #[test]
    fn erroneous_hex_digit() {
        assert_eq!(from_hex_digit('\x2F'), None);
        assert_eq!(from_hex_digit('\x3A'), None);

        assert_eq!(from_hex_digit('\x40'), None);
        assert_eq!(from_hex_digit('\x5B'), None);

        assert_eq!(from_hex_digit('\x60'), None);
        assert_eq!(from_hex_digit('\x7B'), None);
    }
}
