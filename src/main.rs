mod hexbuf;
mod message;
mod packet;
mod slice_display;
mod error;

use rumqttc::v5::mqttbytes::QoS;

use crate::slice_display::SliceDisplay;
use rumqttc::v5::{AsyncClient, MqttOptions};
use serde::Deserialize;
use std::error::Error;
use std::time::Duration;
use tracing::{debug, error, info};
use crate::hexbuf::HexBuf;
use crate::message::Message;

#[derive(Deserialize, Debug)]
struct Config {
    #[serde(default = "default_host")]
    pub mqtt_host: String,
    #[serde(default = "default_port")]
    pub mqtt_port: u16,
    #[serde(default = "default_topic")]
    pub mqtt_topic: String,
    #[serde(default = "nano_id::base64::<64>")]
    pub mqtt_id: String,
    #[serde(default = "default_key")]
    pub key: HexBuf<16>,
}

#[inline]
fn default_host() -> String {
    String::from("helium2.disk91.com")
}

#[inline]
fn default_topic() -> String {
    String::from("isima/gregory")
}

#[inline]
const fn default_port() -> u16 {
    443
}

#[inline]
const fn default_key() -> HexBuf<16> {
    HexBuf::new([
        0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07,
        0x08, 0x09, 0x0A, 0x0B, 0x0C, 0x0D, 0x0E, 0x0F,
    ])
}

#[tokio::main(worker_threads = 1)]
async fn main() {
    let subscriber = tracing_subscriber::FmtSubscriber::builder()
        .with_max_level(tracing::Level::TRACE)
        .finish();

    tracing::subscriber::set_global_default(subscriber).expect("setting default subscriber failed");

    if let Err(e) = run().await {
        error!("{e}");
    }
}

fn gen_mqtt_options(config: &Config) -> MqttOptions {
    let mut ret = MqttOptions::new(
        config.mqtt_id.as_str(),
        config.mqtt_host.as_str(),
        config.mqtt_port,
    );
    ret.set_keep_alive(Duration::from_secs(5));

    ret
}

async fn run() -> Result<(), Box<dyn Error>> {
    let config: Config = envy::from_env()?;

    let options = gen_mqtt_options(&config);

    debug!("MQTT Options {:?}", options);
    info!(
        "Trying connecting to mqtt://{}@{}:{}/{}",
        config.mqtt_id, config.mqtt_host, config.mqtt_port, config.mqtt_topic
    );

    let (client, mut eventloop) = AsyncClient::new(options, 10);

    client
        .subscribe(config.mqtt_topic, QoS::AtMostOnce)
        .await
        .unwrap();

    loop {
        if let rumqttc::v5::Event::Incoming(rumqttc::v5::Incoming::Publish(published)) =
            eventloop.poll().await?
        {
            let message = published.payload.as_ref();

            if let Err(e) = handle_message(message, &config.key) {
                error!("{}", e);
            }
        }
    }
}

fn handle_message(message : &[u8], key : &[u8; 16]) -> Result<(), Box<dyn Error>> {
    debug!("Receiving message: {}", SliceDisplay::new(message));

    let payload : Message = serde_json::from_slice(message)?;

    debug!("{:?}", payload);

    let decrypted = payload.decrypt_data(key)?;

    let decoded = f32::from_be_bytes(TryFrom::try_from(decrypted.as_slice())?);

    info!("Current temperature: {}°C", decoded);

    Ok(())
}

#[cfg(test)]
mod tests {
    use std::ops::Deref;
    use crate::default_key;
    use crate::hexbuf::HexBuf;
    use crate::message::Message;

    const KEY : HexBuf<16> = default_key();

    #[test]
    fn from_payload_to_data() {
        let message = b"{\
            \"app_eui\": \"6081F9A78A08B2B0\",\
            \"dev_eui\": \"393237396C396A10\",\
            \"payload\": \"AAAAANMo2XInBHQX\"\
        }";

        let payload : Message = serde_json::from_slice(message).unwrap();

        let payload = payload.decrypt_data(KEY.deref()).unwrap();

        assert_eq!(payload.as_slice(), b"ADHD");
    }
}