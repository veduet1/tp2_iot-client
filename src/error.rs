use std::fmt::{Debug, Display, Formatter};

pub type Result<T> = std::result::Result<T, Error>;

pub const fn check_slice_length<T>(slice : &[T], length : usize) -> Result<()> {
    if slice.len() < length {
        Err(Error::InvalidSizeError {
            given: slice.len(),
            expected: length
        })
    } else {
        Ok(())
    }
}

pub enum Error {
    InvalidSizeError {
        given: usize,
        expected: usize,
    },
    InvalidCheckSum {
        given: u32,
        expected: u32,
    },
}

impl Debug for Error {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        match self {
            Error::InvalidSizeError { given, expected } => {
                write!(f, "InvalidSizeError {{ given: {given}, expected: {expected} }}")
            },
            Error::InvalidCheckSum { given, expected } => {
                write!(f, "InvalidCheckSum {{ given: 0x{given:08x}, expected: 0x{expected:08x} }}")
            }
        }
    }
}

impl Display for Error {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        match self {
            Error::InvalidSizeError { given, expected } => {
                write!(f, "The slice size is invalid, expected {expected}, given {given}")
            }
            Error::InvalidCheckSum { given, expected } => {
                write!(f, "The slice data is inconsistent, expected checksum is {expected:08x} but the given one is {given:08x}")
            }
        }
    }
}

impl std::error::Error for Error {}