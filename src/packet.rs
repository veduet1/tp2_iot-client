use aes::Aes128;
use aes::cipher::{ KeyIvInit, StreamCipher };
use ctr::Ctr32LE;
use crate::error::*;

type AesCtr128 = Ctr32LE<Aes128>;

const CRC : crc::Crc<u32> = crc::Crc::<u32>::new(&crc::CRC_32_ISO_HDLC);

fn bytes_add_assign(a : &mut [u8], b : &[u8]) -> bool {
    let mut restrain = false;
    let max_calc = usize::min(a.len(), b.len());

    for i in 0..max_calc {
        let (res, next) = a[i].overflowing_add(b[i]);

        if restrain {
            (a[i], restrain) = res.overflowing_add(1);
        } else {
            a[i] = res;
            restrain = next;
        }
    }

    for i in max_calc..a.len() {
        if restrain {
            (a[i], restrain) = a[i].overflowing_add(1);
        } else {
            break;
        }
    }

    restrain
}

fn add_counter_to_iv(iv : &mut [u8; 16], counter : u32) -> bool {
    bytes_add_assign(iv.as_mut_slice(), counter.to_le_bytes().as_slice())
}

pub struct RawPacket<'a> {
    counter : u32,
    crc : u32,
    data : &'a [u8]
}

impl<'a> RawPacket<'a> {
    fn u32_from_slice(from : &[u8]) -> Result<u32> {
        let array : [u8; 4] = TryFrom::try_from(from).unwrap();

        Ok(u32::from_be_bytes(array))
    }

    // pub fn get_data(&self) -> &[u8] {
    //     self.data
    // }

    pub fn check_crc(&self) -> Result<()> {
        let crc = CRC.checksum(self.data);

        if crc == self.crc {
            Ok(())
        } else {
            Err(Error::InvalidCheckSum {
                expected: crc,
                given: self.crc,
            })
        }
    }

    pub fn decrypt_data(&self, key : &[u8; 16], iv : &[u8; 16]) -> Result<Vec<u8>> {
        self.check_crc()?;

        let mut iv = iv.clone();

        // Il est 5 heures, flemme de réfléchir
        iv.reverse();

        add_counter_to_iv(&mut iv, self.counter);

        iv.reverse();

        let mut ctr : AesCtr128 = KeyIvInit::new(key.into(), &iv.into());

        let mut ret = Vec::new();
        ret.resize(self.data.len(), 0u8);

        ctr.apply_keystream_b2b(self.data, ret.as_mut_slice()).unwrap();

        Ok(ret)
    }
}

impl<'a> TryFrom<&'a [u8]> for RawPacket<'a> {
    type Error = Error;

    fn try_from(value: &'a [u8]) -> Result<Self> {
        check_slice_length(value, 8)?;

        let counter = Self::u32_from_slice(&value[0..4])?;
        let crc     = Self::u32_from_slice(&value[4..8])?;

        let data = &value[8..];

        Ok(Self {
            crc,
            counter,
            data
        })
    }
}

#[cfg(test)]
mod tests {
    use crate::packet::{add_counter_to_iv, RawPacket, CRC, bytes_add_assign};

    const REFERENCE : &'static [u8] = &[
        0x00, 0x00, 0x00, 0x01,
        0x40, 0x39, 0x81, 0x05,
        0x26, 0x08, 0xC9, 0x90,
    ];

    const BAD_CRC : &'static [u8] = &[
        0x00, 0x00, 0x00, 0x01,
        0x40, 0x38, 0x81, 0x05,
        0x26, 0x08, 0xC9, 0x90
    ];

    const BAD_DATA : &'static [u8] = &[
        0x00, 0x00, 0x00, 0x01,
        0x40, 0x39, 0x81, 0x05,
        0x26, 0x08, 0xC8, 0x90
    ];

    const KEY : &'static [u8; 16] = &[
        0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07,
        0x08, 0x09, 0x0A, 0x0B, 0x0C, 0x0D, 0x0E, 0x0F,
    ];

    const IV : &'static [u8; 16] = &[
        0x39, 0x32, 0x37, 0x39, 0x6c, 0x39, 0x6a, 0x10,
        0x60, 0x81, 0xf9, 0xa7, 0x8a, 0x08, 0xb2, 0xb0,
    ];

    #[test]
    fn extract_payload() {
        let raw_packet = RawPacket::try_from(REFERENCE).unwrap();

        assert_eq!(raw_packet.counter, 0x00_00_00_01);
        assert_eq!(raw_packet.    crc, 0x40_39_81_05);
        assert_eq!(raw_packet.   data, &[0x26, 0x08, 0xC9, 0x90]);
    }

    #[test]
    fn crc() {
        let raw_packet = RawPacket::try_from(REFERENCE).unwrap();

        raw_packet.check_crc().unwrap();
    }

    #[test]
    fn bad_crc() {
        let raw_packet = RawPacket::try_from(BAD_CRC).unwrap();

        raw_packet.check_crc().unwrap_err();
    }

    #[test]
    fn bad_data() {
        let raw_packet = RawPacket::try_from(BAD_DATA).unwrap();

        raw_packet.check_crc().unwrap_err();
    }

    #[test]
    fn bytes_add() {
        const A : u128 = 5489162105465189460898405890494;
        const B : u32 = 544216840;

        const R : u128 = A + B as u128;

        let mut buf = A.to_le_bytes();

        add_counter_to_iv(&mut buf, B);

        assert_eq!(R.to_le_bytes(), buf);
    }

    #[test]
    fn overflow_add() {
        const A : u32 = 0xFF_FF_00_01;
        const B : u16 = 0xFF_FF;

        const R : (u32, bool) = A.overflowing_add(B as u32);

        let mut buf_a = A.to_le_bytes();
        let buf_b = B.to_le_bytes();

        assert_eq!(R.1, bytes_add_assign(&mut buf_a, &buf_b));

        assert_eq!(R.0.to_le_bytes(), buf_a);
    }

    #[test]
    fn decrypt_base() {
        let data = &[0x27, 0x04, 0x74, 0x17];

        let raw_packet = RawPacket {
            counter: 0,
            crc: CRC.checksum(data),
            data,
        };

        let data = raw_packet.decrypt_data(KEY, IV).unwrap();

        assert_eq!(data.as_slice(), b"ADHD");
    }

    #[test]
    fn decrypt_count() {
        let data = &[0x51, 0x1A, 0xBA, 0x46];

        let raw_packet = RawPacket {
            counter: 7,
            crc: CRC.checksum(data),
            data,
        };

        let data = raw_packet.decrypt_data(KEY, IV).unwrap();

        assert_eq!(data.as_slice(), b"ADHD");
    }
}